package native_utils

func Pointer[anything any](in anything) *anything {
	return &in
}
