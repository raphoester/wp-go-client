package native_utils

func Pop[anything any](slice []anything) []anything {
	if len(slice) == 0 {
		return slice
	}
	return slice[1:]
}

func GetAndPop[anything any](slice []anything) ([]anything, *anything) {
	if len(slice) == 0 {
		return slice, nil
	}

	value := slice[0]
	return Pop(slice), &value
}
