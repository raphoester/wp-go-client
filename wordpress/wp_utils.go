package wordpress

import "fmt"

func url(baseUrl, uri string) string {
	url := fmt.Sprintf("%s/wp-json%s", baseUrl, uri)
	return url
}
