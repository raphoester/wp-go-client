package wordpress

import "net/http"

type Iauthenticator interface {
	Authenticate(http.Request) *http.Request
}

type basicAuthenticator struct {
	username string
	password string
}

func NewBasicAuthenticator(username, password string) *basicAuthenticator {
	//lint:ignore S1021 _
	var ret Iauthenticator
	ret = &basicAuthenticator{
		username: username,
		password: password,
	}
	return ret.(*basicAuthenticator)
}

func (a *basicAuthenticator) Authenticate(r http.Request) *http.Request {
	r.SetBasicAuth(a.username, a.password)
	return &r
}
