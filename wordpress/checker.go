package wordpress

import (
	"net/http"

	"gitlab.com/raphoester/wp-go-client/errors"
	"gitlab.com/raphoester/wp-go-client/internal/native_utils"
)

func (w *wp) CheckAuth(baseUrl string, authenticator Iauthenticator) (*bool, *errors.RestError) {
	res, err := w.requestHelper.RawRequest(baseUrl, "/wp/v2/users/me", "GET", nil, authenticator)
	if err != nil {
		w.logger.Info("failed doing request | %s", err.Error())
		return nil, errors.NewInternalServerError()
	}

	if res.StatusCode != http.StatusOK {
		w.logger.Info("auth is not properly set up (response code %d)", res.StatusCode)
		return native_utils.Pointer(false), nil
	}

	w.logger.Debug("auth check ok")
	return native_utils.Pointer(true), nil
}
