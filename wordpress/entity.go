package wordpress

// https://developer.wordpress.org/rest-api/reference/posts/#arguments-2
type CreateArticleInput struct {
	// 	The date the object was published, in the site's timezone.
	Date *string `json:"date,omitempty"`

	// The date the object was published, as GMT.
	DateGmt *string `json:"date_gmt,omitempty"`

	// An alphanumeric identifier for the object unique to its type.
	//
	// One of: publish, future, draft, pending, private
	Slug *string `json:"slug,omitempty"`

	// A named status for the object.
	//
	// One of: publish, future, draft, pending, private
	Status *string `json:"status,omitempty"`

	// A password to protect access to the content and excerpt.
	Password *string `json:"password,omitempty"`

	// The title for the object.
	Title interface{} `json:"title,omitempty"`

	// The content for the object.
	Content interface{} `json:"content,omitempty"`

	// The ID for the author of the object.
	Author *int `json:"author,omitempty"`

	// The excerpt for the object.
	Excerpt interface{} `json:"excerpt,omitempty"`

	// The ID of the featured media for the object.
	FeaturedMedia *int `json:"featured_media,omitempty"`

	// Whether or not comments are open on the object.
	//
	// One of: open, closed
	CommentStatus *string `json:"comment_status,omitempty"`

	// 	Whether or not the object can be pinged.
	//
	// One of: open, closed
	PingStatus *string `json:"ping_status,omitempty"`

	// 	The format for the object.
	//
	// One of: standard, aside, chat, gallery, link, image,
	// quote, status, video, audio
	Format *string `json:"format,omitempty"`

	// Meta fields.
	Meta interface{} `json:"meta,omitempty"`

	// Whether or not the object should be treated as sticky.
	Sticky *bool `json:"bool,omitempty"`

	// The theme file to use to display the object.
	Template *string `json:"template,omitempty"`

	// The terms assigned to the object in the category taxonomy.
	Categories []interface{} `json:"categories,omitempty"`

	// The terms assigned to the object in the post_tag taxonomy.
	Tags []interface{} `json:"tags,omitempty"`
}

// Context string `json:"context"`
// Page    int    `json:"page"`
