package wordpress

import (
	"gitlab.com/raphoester/wp-go-client/errors"
	"gitlab.com/raphoester/wp-go-client/internal/native_utils"
	"gitlab.com/raphoester/wp-go-client/log"
)

type wpMock struct {
	logger log.Ilogger
	mocks  []WpMock
}

func NewWpMock(logger log.Ilogger) *wpMock {
	//lint:ignore S1021 _
	var ret Iwp
	ret = &wpMock{
		logger: logger,
	}
	return ret.(*wpMock)
}

type WpMock struct {
	Error *errors.RestError
	Bool  *bool
}

func (w *wpMock) AddMock(mock WpMock) {
	w.mocks = append(w.mocks, mock)
}

func (w *wpMock) WantFirstMock() *WpMock {
	if len(w.mocks) > 0 {
		var ret *WpMock
		w.mocks, ret = native_utils.GetAndPop(w.mocks)
		return ret
	}

	w.logger.Error("no mocks available")
	return nil
}

func (w *wpMock) CheckAuth(fqdn string, auth Iauthenticator) (*bool, *errors.RestError) {
	mock := w.WantFirstMock()

	return mock.Bool, mock.Error
}

func (w *wpMock) PostArticle(base string, article CreateArticleInput, auth Iauthenticator) *errors.RestError {
	mock := w.WantFirstMock()
	return mock.Error
}
