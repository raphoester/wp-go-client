package wordpress

import "gitlab.com/raphoester/wp-go-client/errors"

func (w *wp) PostArticle(baseUrl string, article CreateArticleInput, authenticator Iauthenticator) *errors.RestError {

	err := w.requestHelper.CleanRequest(baseUrl, "/wp/v2/posts", "POST", article, authenticator, nil)
	if err != nil {
		w.logger.Info("error sending request to api | %s", err.Error())
		return errors.NewInternalServerError()
	}

	return nil
}
