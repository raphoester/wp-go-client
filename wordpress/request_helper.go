package wordpress

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"

	"net/http"

	"gitlab.com/raphoester/wp-go-client/errors"
	"gitlab.com/raphoester/wp-go-client/log"
)

type IrequestHelper interface {
	CleanRequest(baseUrl, uri, verb string,
		body any, auth Iauthenticator, destination any) *errors.RestError
	RawRequest(baseUrl, uri, verb string,
		body []byte, auth Iauthenticator) (*http.Response, error)
}

type requestHelper struct {
	logger     log.Ilogger
	httpClient http.Client
}

func NewRequestHelper(logger log.Ilogger, client http.Client) *requestHelper {

	//lint:ignore S1021 _
	var ret IrequestHelper
	ret = &requestHelper{
		logger:     logger,
		httpClient: client,
	}

	return ret.(*requestHelper)
}

func (h *requestHelper) CleanRequest(baseUrl, uri, verb string,
	body any, authenticator Iauthenticator, destination any) *errors.RestError {

	var jsonBody []byte
	if body != nil {
		var nativeErr error
		jsonBody, nativeErr = json.Marshal(body)
		if nativeErr != nil {
			h.logger.Error("failed marshaling body content to json | %s", nativeErr.Error())
		}
	}

	res, err := h.RawRequest(baseUrl, uri, verb, jsonBody, authenticator)
	if err != nil {
		return errors.NewInternalServerError()
	}

	if res.StatusCode < 200 || res.StatusCode > 299 {
		h.logger.Info("invalid status code %d returned by wordpress api", res.StatusCode)
		responseBody, err := io.ReadAll(res.Body)
		if err != nil {
			h.logger.Info("failed reading response body | %s", err.Error())
		} else {
			h.logger.Info("body : %s", string(responseBody))
		}
		return errors.NewRestError(res.StatusCode, "")
	}

	if err := json.NewDecoder(res.Body).Decode(&destination); err != nil {
		h.logger.Error("failed reading response body | %s", err.Error())
		return errors.NewInternalServerError()
	}

	return nil
}

func (h *requestHelper) RawRequest(baseUrl, uri, verb string,
	body []byte, auth Iauthenticator) (*http.Response, error) {
	url := url(baseUrl, uri)

	request, err := http.NewRequest(verb, url, bytes.NewReader(body))
	if err != nil {
		h.logger.Error("failed crafting request | %s", err.Error())
		return nil, fmt.Errorf("invalid request input")
	}

	request.Header.Add("Content-Type", "application/json")

	req := auth.Authenticate(*request)
	res, err := h.httpClient.Do(req)
	if err != nil {
		h.logger.Error("failed executing authentication request | %s", err.Error())
		return nil, err
	}

	return res, err
}
