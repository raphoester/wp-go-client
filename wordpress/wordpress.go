package wordpress

import (
	"gitlab.com/raphoester/wp-go-client/errors"
	"gitlab.com/raphoester/wp-go-client/log"
)

type Iwp interface {
	PostArticle(base string, article CreateArticleInput, auth Iauthenticator) *errors.RestError
	CheckAuth(baseUrl string, auth Iauthenticator) (*bool, *errors.RestError)
}

type wp struct {
	logger        log.Ilogger
	requestHelper IrequestHelper
}

func NewWp(logger log.Ilogger, helper IrequestHelper) *wp {
	//lint:ignore S1021 _
	var ret Iwp
	ret = &wp{
		logger:        logger,
		requestHelper: helper,
	}
	return ret.(*wp)
}
