package log

import (
	"fmt"
	"runtime"
)

func getSecondCallerFunctionName() string {
	pc, _, _, ok := runtime.Caller(3)
	details := runtime.FuncForPC(pc)
	if ok && details != nil {
		// return strings.Split(details.Name(), ".")[1]
		return details.Name()
	}

	return ""
}

func formatText(kindMessage string, callerFunc string, text string) string {
	return fmt.Sprintf("[%s] %s : %s", kindMessage, callerFunc, text)
}
