package log

func getKindMessageStdout(emergency int) string {
	message := ""
	switch emergency {
	case 1:
		message = "CORE"
	case 2:
		message = "INTERNAL ERROR"
	case 3:
		message = "INFO"
	case 4:
		message = "DEBUG"
	}
	return message
}

func getKindMessageStderr(emergency int) string {
	message := ""
	switch emergency {
	case 1:
		message = "FATAL"
		// ?? what else idk
	}
	return message
}
